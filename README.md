FabAccess-API-cs
===

FabAccess API in C# with Tests. 

**This API is not ready for use**

You will find some rudimentary docs at [docs.fab-access.org](https://docs.fab-access.org/books/schnittstellen-und-apis/page/fabaccess-api#bkmrk-fabaccess-api-cs). 

See also prior [FabAccess-API](https://gitlab.com/fabinfra/fabaccess/fabaccess-api).