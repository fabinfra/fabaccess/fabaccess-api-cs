﻿using Capnp.Rpc;
using FabAccessAPI;
using FabAccessAPI.Schema;
using NUnit.Framework;

namespace FabAccessAPI_Test;

public class ResourcenRaum
{
    [TestCase("nbecker", "solderingstation-1")]
    public async Task ClaimPowerableResource(string username, string resourcename)
    {
        API api = new API();
        ConnectionData connectionData = TestEnv.CreateConnetionData(username);
        await api.Connect(connectionData).ConfigureAwait(false);

        Resource resource = (await api.Session.Resources.GetByName(resourcename).ConfigureAwait(false)).Item1;

        IClaim claim = (await resource.Claim.Claim(null).ConfigureAwait(false)).Ok;

        Map<IReadOnlyList<byte>, object> traits = await claim.Traits().ConfigureAwait(false);

        byte[] oid_trait_powerable = OID.OidStringToByteArray("1.3.6.1.4.1.61783.612.1.1");

        object pointer = traits.Entries.First(x => x.Key == oid_trait_powerable).Value;
        
        ITraitPowerable traitPowerable = ((Proxy)pointer).Cast<ITraitPowerable>(false);

        StatePowerable statePowerable = await traitPowerable.GetState().ConfigureAwait(false);
        if(statePowerable.which != StatePowerable.WHICH.Off)
        {
            Assert.Inconclusive("start state is not the one expected");
        }
        
        traitPowerable = (await traitPowerable.TurnOn().ConfigureAwait(false)).Ok;

        Assert.That(await traitPowerable.GetState().ConfigureAwait(false), Is.EqualTo(StatePowerable.WHICH.On));

        traitPowerable = (await traitPowerable.TurnOff().ConfigureAwait(false)).Ok;

        Assert.That(await traitPowerable.GetState().ConfigureAwait(false), Is.EqualTo(StatePowerable.WHICH.Off));
    
        await claim.Disown().ConfigureAwait(false);
    }
}
