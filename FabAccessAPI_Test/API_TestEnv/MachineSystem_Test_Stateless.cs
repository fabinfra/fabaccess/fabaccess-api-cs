﻿// using FabAccessAPI;
// using FabAccessAPI.Schema;
// using NUnit.Framework;
// using NUnit.Framework.Legacy;
// using System.Collections.Generic;
// using System.Threading.Tasks;

// namespace FabAccessAPI_Test.API_TestEnv
// {
//     [TestFixture, Parallelizable(ParallelScope.Children)]
//     [Order(1)]
//     public class MachineSystem_Test_Stateless
//     {
//         [TestCase("Admin1", true)]
//         [TestCase("Admin2", true)]
//         [TestCase("ManagerA1", true)]
//         [TestCase("ManagerA2", true)]
//         [TestCase("ManagerB1", true)]
//         [TestCase("ManagerB2", true)]
//         [TestCase("ManagerC1", true)]
//         [TestCase("ManagerC2", true)]
//         [TestCase("ManagerABC1", true)]
//         [TestCase("ManagerABC2", true)]
//         [TestCase("MakerA1", true)]
//         [TestCase("MakerA2", true)]
//         [TestCase("MakerB1", true)]
//         [TestCase("MakerB2", true)]
//         [TestCase("MakerC1", true)]
//         [TestCase("MakerC2", true)]
//         [TestCase("MakerABC1", true)]
//         [TestCase("MakerABC2", true)]
//         [TestCase("GuestA1", true)]
//         [TestCase("GuestA2", true)]
//         [TestCase("GuestB1", true)]
//         [TestCase("GuestB2", true)]
//         [TestCase("GuestC1", true)]
//         [TestCase("GuestC2", true)]
//         [TestCase("GuestABC1", true)]
//         [TestCase("GuestABC2", true)]
//         [TestCase("MakerQRA", true)]
//         [TestCase("MakerQRB", true)]
//         [TestCase("MakerQRC", true)]
//         [Order(2)]
//         public async Task AccessMachineSystem_Info(string username, bool expectAllow)
//         {
//             API api = new API();
//             ConnectionData connectionData = TestEnv.CreateConnetionData(username);
//             await api.Connect(connectionData);

//             ClassicAssert.AreEqual(expectAllow, !((MachineSystem.InfoInterface_Proxy)api.Session.MachineSystem.Info).IsNull);
//         }

//         [TestCase("Admin1", 15)]
//         [TestCase("ManagerA1", 5)]
//         [TestCase("ManagerB1", 5)]
//         [TestCase("ManagerC1", 5)]
//         [TestCase("ManagerABC1", 15)]
//         [TestCase("MakerA1", 5)]
//         [TestCase("MakerB1", 5)]
//         [TestCase("MakerC1", 5)]
//         [TestCase("MakerABC1", 15)]
//         [TestCase("GuestA1", 5)]
//         [TestCase("GuestB1", 5)]
//         [TestCase("GuestC1", 5)]
//         [TestCase("GuestABC1", 15)]
//         [TestCase("MakerQRA", 0)]
//         [TestCase("MakerQRB", 0)]
//         [TestCase("MakerQRC", 0)]
//         [Order(3)]
//         public async Task ListMachines(string username, int expectedMachineCount)
//         {
//             API api = new API();
//             ConnectionData connectionData = TestEnv.CreateConnetionData(username);
//             await api.Connect(connectionData);

//             IReadOnlyList<Machine> machine_list = await api.Session.MachineSystem.Info.GetMachineList().ConfigureAwait(false);

//             int result = machine_list.Count;

//             await api.Disconnect();

//             ClassicAssert.AreEqual(expectedMachineCount, result);
//         }

//         [TestCase("Admin1", "MachineA1", true)]
//         [TestCase("Admin1", "MachineB1", true)]
//         [TestCase("Admin1", "MachineC1", true)]
//         [TestCase("ManagerA1", "MachineA1", true)]
//         [TestCase("ManagerA1", "MachineB1", false)]
//         [TestCase("ManagerA1", "MachineC1", false)]
//         [TestCase("ManagerB1", "MachineA1", false)]
//         [TestCase("ManagerB1", "MachineB1", true)]
//         [TestCase("ManagerB1", "MachineC1", false)]
//         [TestCase("ManagerC1", "MachineA1", false)]
//         [TestCase("ManagerC1", "MachineB1", false)]
//         [TestCase("ManagerC1", "MachineC1", true)]
//         [TestCase("ManagerABC1", "MachineA1", true)]
//         [TestCase("ManagerABC1", "MachineB1", true)]
//         [TestCase("ManagerABC1", "MachineC1", true)]
//         [TestCase("MakerA1", "MachineA1", true)]
//         [TestCase("MakerB1", "MachineB1", true)]
//         [TestCase("MakerC1", "MachineC1", true)]
//         [TestCase("GuestA1", "MachineA1", true)]
//         [TestCase("GuestB1", "MachineB1", true)]
//         [TestCase("GuestC1", "MachineC1", true)]
//         [TestCase("MakerQRA", "MachineA1", true)]
//         [TestCase("MakerQRB", "MachineB1", true)]
//         [TestCase("MakerQRC", "MachineC1", true)]
//         [Order(4)]
//         public async Task GetMachineByName(string username, string machineName, bool expectedAllow)
//         {
//             API api = new API();
//             ConnectionData connectionData = TestEnv.CreateConnetionData(username);
//             await api.Connect(connectionData);

//             Optional<Machine> optional = await api.Session.MachineSystem.Info.GetMachine(machineName).ConfigureAwait(false);

//             bool result = optional.Just != null;

//             await api.Disconnect();

//             ClassicAssert.AreEqual(expectedAllow, result);
//         }

//         [TestCase("Admin1", "MachineX")]
//         [TestCase("Admin1", "urn:fabaccess:resource:MachineA1")]
//         [Order(5)]
//         public async Task GetMachineByName_WrongName(string username, string machineName)
//         {
//             API api = new API();
//             ConnectionData connectionData = TestEnv.CreateConnetionData(username);
//             await api.Connect(connectionData);

//             Optional<Machine> optional = await api.Session.MachineSystem.Info.GetMachine(machineName).ConfigureAwait(false);

//             await api.Disconnect();

//             ClassicAssert.IsNull(optional.Just);
//         }

//         [TestCase("Admin1", "urn:fabaccess:resource:MachineA1", true)]
//         [TestCase("Admin1", "urn:fabaccess:resource:MachineB1", true)]
//         [TestCase("Admin1", "urn:fabaccess:resource:MachineC1", true)]
//         [TestCase("ManagerA1", "urn:fabaccess:resource:MachineA1", true)]
//         [TestCase("ManagerA1", "urn:fabaccess:resource:MachineB1", false)]
//         [TestCase("ManagerA1", "urn:fabaccess:resource:MachineC1", false)]
//         [TestCase("ManagerB1", "urn:fabaccess:resource:MachineA1", false)]
//         [TestCase("ManagerB1", "urn:fabaccess:resource:MachineB1", true)]
//         [TestCase("ManagerB1", "urn:fabaccess:resource:MachineC1", false)]
//         [TestCase("ManagerC1", "urn:fabaccess:resource:MachineA1", false)]
//         [TestCase("ManagerC1", "urn:fabaccess:resource:MachineB1", false)]
//         [TestCase("ManagerC1", "urn:fabaccess:resource:MachineC1", true)]
//         [TestCase("ManagerABC1", "urn:fabaccess:resource:MachineA1", true)]
//         [TestCase("ManagerABC1", "urn:fabaccess:resource:MachineB1", true)]
//         [TestCase("ManagerABC1", "urn:fabaccess:resource:MachineC1", true)]
//         [TestCase("MakerA1", "urn:fabaccess:resource:MachineA1", true)]
//         [TestCase("MakerB1", "urn:fabaccess:resource:MachineB1", true)]
//         [TestCase("MakerC1", "urn:fabaccess:resource:MachineC1", true)]
//         [TestCase("GuestA1", "urn:fabaccess:resource:MachineA1", true)]
//         [TestCase("GuestB1", "urn:fabaccess:resource:MachineB1", true)]
//         [TestCase("GuestC1", "urn:fabaccess:resource:MachineC1", true)]
//         [TestCase("MakerQRA", "urn:fabaccess:resource:MachineA1", true)]
//         [TestCase("MakerQRB", "urn:fabaccess:resource:MachineB1", true)]
//         [TestCase("MakerQRC", "urn:fabaccess:resource:MachineC1", true)]

//         [Order(6)]
//         public async Task GetMachineByURN(string username, string urn, bool expectedAllow)
//         {
//             API api = new API();
//             ConnectionData connectionData = TestEnv.CreateConnetionData(username);
//             await api.Connect(connectionData);

//             Optional<Machine> optional = await api.Session.MachineSystem.Info.GetMachineURN(urn).ConfigureAwait(false);
//             bool result = optional.Just != null;

//             await api.Disconnect();

//             ClassicAssert.AreEqual(expectedAllow, result);
//         }

//         [TestCase("Admin1", "urn:fabaccess:resource:MachineX")]
//         [TestCase("Admin1", "MachineA1")]
//         [TestCase("Admin1", "something")]
//         [Order(7)]
//         public async Task GetMachineByURN_WrongURN(string username, string urn)
//         {
//             API api = new API();
//             ConnectionData connectionData = TestEnv.CreateConnetionData(username);
//             await api.Connect(connectionData);

//             Optional<Machine> optional = await api.Session.MachineSystem.Info.GetMachineURN(urn).ConfigureAwait(false);

//             await api.Disconnect();

//             ClassicAssert.IsNull(optional.Just);
//         }
//     }

// }
