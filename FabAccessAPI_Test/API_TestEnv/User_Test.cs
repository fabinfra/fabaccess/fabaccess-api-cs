﻿// using FabAccessAPI;
// using FabAccessAPI.Schema;
// using NUnit.Framework;
// using NUnit.Framework.Legacy;
// using System.Collections.Generic;
// using System.Threading.Tasks;

// namespace FabAccessAPI_Test.API_TestEnv
// {
//     [TestFixture]
//     public class User_Test
//     {
//         #region SetUp
//         [SetUp]
//         public async Task SetUp()
//         {
//             API api = new API();
//             ConnectionData connectionData = TestEnv.CreateConnetionData("Admin1");
//             await api.Connect(connectionData);

//             IReadOnlyList<User> user_list = await api.Session.UserSystem.Manage.GetUserList().ConfigureAwait(false);

//             List<Task> tasks = new List<Task>();
//             foreach (User u in user_list)
//             {
//                 if (u.Username.StartsWith("New"))
//                 {
//                     tasks.Add(api.Session.UserSystem.Manage.RemoveUser(u));
//                 }
//             }

//             await Task.WhenAll(tasks);
//         }
//         #endregion

//         [TestCase("Admin1", "NewMakerA1", "UseA", "ReadA", "DiscloseA")]
//         [Order(1)]
//         [Ignore("Deprecated")]
//         public async Task AddRoles(string username, string username2, params string[] roles)
//         {
//             API api = new API();
//             ConnectionData connectionData = TestEnv.CreateConnetionData(username);
//             await api.Connect(connectionData);

//             await api.Session.UserSystem.Manage.AddUser(username2, TestEnv.PASSWORD);

//             User user = (await api.Session.UserSystem.Search.GetUserByName(username2).ConfigureAwait(false)).Just;

//             foreach(string s in roles)
//             {
//                 await user.Admin.AddRole(new Role() { Name = s }).ConfigureAwait(false);
//             }
            
//             user = (await api.Session.UserSystem.Search.GetUserByName(username2).ConfigureAwait(false)).Just;
//             List<Role> user_roles = new List<Role>(await user.Info.ListRoles().ConfigureAwait(false));

//             await api.Disconnect();

//             ClassicAssert.Multiple(() =>
//             {
//                 ClassicAssert.AreEqual(3, user_roles.Count);
//                 foreach (string s in roles)
//                 {
//                     ClassicAssert.IsTrue(user_roles.Exists(x => x.Name == s));
//                 }
//             });
//         }
//     }
// }
