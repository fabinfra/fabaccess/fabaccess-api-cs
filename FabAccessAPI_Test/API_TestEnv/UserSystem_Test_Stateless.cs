﻿// using FabAccessAPI;
// using FabAccessAPI.Schema;
// using NUnit.Framework;
// using NUnit.Framework.Legacy;
// using System.Threading.Tasks;

// namespace FabAccessAPI_Test.API_TestEnv
// {
//     [TestFixture, Parallelizable(ParallelScope.Children)]
//     [Order(1)]
//     public class UserSystem_Test_Stateless
//     {
//         [TestCase("Admin1", true)]
//         [TestCase("ManagerA1", true)]
//         [TestCase("MakerA1", true)]
//         [TestCase("GuestA1", true)]
//         [Order(2)]
//         public async Task InfoInterface(string username, bool expectInterface)
//         {
//             API api = new API();
//             ConnectionData connectionData = TestEnv.CreateConnetionData(username);
//             await api.Connect(connectionData);

//             UserSystem.InfoInterface_Proxy infoInterface = (UserSystem.InfoInterface_Proxy)api.Session.UserSystem.Info;

//             bool result = !infoInterface.IsNull;

//             await api.Disconnect();

//             ClassicAssert.AreEqual(expectInterface, result);
//         }

//         [TestCase("Admin1", true)]
//         [TestCase("ManagerA1", true)]
//         [TestCase("MakerA1", false)]
//         [TestCase("GuestA1", false)]
//         [Order(3)]
//         public async Task ManageInterface(string username, bool expectInterface)
//         {
//             API api = new API();
//             ConnectionData connectionData = TestEnv.CreateConnetionData(username);
//             await api.Connect(connectionData);

//             UserSystem.ManageInterface_Proxy manageInterface = (UserSystem.ManageInterface_Proxy)api.Session.UserSystem.Manage;

//             bool result = !manageInterface.IsNull;

//             await api.Disconnect();

//             ClassicAssert.AreEqual(expectInterface, result);
//         }

//         [TestCase("Admin1", true)]
//         [TestCase("ManagerA1", true)]
//         [TestCase("MakerA1", false)]
//         [TestCase("GuestA1", false)]
//         [Order(3)]
//         public async Task SearchInterface(string username, bool expectInterface)
//         {
//             API api = new API();
//             ConnectionData connectionData = TestEnv.CreateConnetionData(username);
//             await api.Connect(connectionData);

//             UserSystem.SearchInterface_Proxy searchInterface = (UserSystem.SearchInterface_Proxy)api.Session.UserSystem.Search;

//             bool result = !searchInterface.IsNull;

//             await api.Disconnect();

//             ClassicAssert.AreEqual(expectInterface, result);
//         }

//         [TestCase("Admin1")]
//         [TestCase("ManagerA1")]
//         [TestCase("MakerA1")]
//         [TestCase("GuestA1")]
//         [Order(4)]
//         public async Task GetUserSelf(string username)
//         {
//             API api = new API();
//             ConnectionData connectionData = TestEnv.CreateConnetionData(username);
//             await api.Connect(connectionData);

//             User user = await api.Session.UserSystem.Info.GetUserSelf().ConfigureAwait(false);

//             await api.Disconnect();

//             ClassicAssert.IsNotNull(user);
//         }

//         [TestCase("Admin1", "Admin1")]
//         [TestCase("Admin1", "MakerA1")]
//         [TestCase("Admin1", "GuestA1")]
//         [Order(4)]
//         public async Task GetUserByUsername(string username, string username2)
//         {
//             API api = new API();
//             ConnectionData connectionData = TestEnv.CreateConnetionData(username);
//             await api.Connect(connectionData);

//             User user = (await api.Session.UserSystem.Search.GetUserByName(username2).ConfigureAwait(false)).Just;

//             await api.Disconnect();

//             ClassicAssert.IsNotNull(user);
//         }

//         [TestCase("Admin1", "UnknownUser")]
//         [Order(5)]
//         public async Task GetUserByUsername_NotExist(string username, string username2)
//         {
//             API api = new API();
//             ConnectionData connectionData = TestEnv.CreateConnetionData(username);
//             await api.Connect(connectionData);

//             User user = (await api.Session.UserSystem.Search.GetUserByName(username2).ConfigureAwait(false)).Just;

//             await api.Disconnect();

//             ClassicAssert.IsNull(user);
//         }
//     }
// }
