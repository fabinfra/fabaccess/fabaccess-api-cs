using FabAccessAPI;
using NUnit.Framework;
using System.Threading.Tasks;
using FabAccessAPI.Schema;
using NUnit.Framework.Legacy;
using System.Runtime.Serialization;
using Capnp.Rpc;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Serialization;

namespace FabAccessAPI_Test.API_SpecTests
{
    [TestFixture, Parallelizable(ParallelScope.Children)]
    public class Bootstrap
    {
        private bool _RemoteCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        private Stream _InjectSSL(Stream tcpstream)
        {
            SslStream sslStream = new SslStream(tcpstream, false, new RemoteCertificateValidationCallback(_RemoteCertificateValidationCallback));
            sslStream.ReadTimeout = 5000;
            sslStream.AuthenticateAsClient("bffhd");
            sslStream.ReadTimeout = -1;
            
            return sslStream;
        }
        
        [TestCase(1, 0)]
        public async Task getAPIVersion(int major, int minor)
        {
            TcpRpcClient tcpRpcClient = new TcpRpcClient();      
            tcpRpcClient.InjectMidlayer(_InjectSSL);

            tcpRpcClient.Connect(TestEnv.TESTSERVER, TestEnv.TESTSERVER_PORT);
            await tcpRpcClient.WhenConnected;

            IBootstrap _Bootstrap = tcpRpcClient.GetMain<IBootstrap>();

            FabAccessAPI.Schema.Version data = await _Bootstrap.GetAPIVersion().ConfigureAwait(false);
            
            FabAccessAPI.Schema.Version expected = new FabAccessAPI.Schema.Version()
            {
                Major = major,
                Minor = minor
            };

            tcpRpcClient.Dispose();

            Assert.That(data, Is.EqualTo(expected));
        }

        [TestCase("bffhd", "TODO")]
        public async Task getServerRelease(string name, string release)
        {
            TcpRpcClient tcpRpcClient = new TcpRpcClient();      
            tcpRpcClient.InjectMidlayer(_InjectSSL);

            tcpRpcClient.Connect(TestEnv.TESTSERVER, TestEnv.TESTSERVER_PORT);
            await tcpRpcClient.WhenConnected;

            IBootstrap _Bootstrap = tcpRpcClient.GetMain<IBootstrap>();

            (string data_name, string data_release) = await _Bootstrap.GetServerRelease().ConfigureAwait(false);
            
            tcpRpcClient.Dispose();

            Assert.That(data_name, Is.EqualTo(name));
            Assert.That(data_release, Is.EqualTo(release));
        }

        [TestCase("FabAccessTest", "test.fab-access.space")]
        public async Task getServerInfo(string spacename, string instanceurl)
        {
            TcpRpcClient tcpRpcClient = new TcpRpcClient();      
            tcpRpcClient.InjectMidlayer(_InjectSSL);

            tcpRpcClient.Connect(TestEnv.TESTSERVER, TestEnv.TESTSERVER_PORT);
            await tcpRpcClient.WhenConnected;

            IBootstrap _Bootstrap = tcpRpcClient.GetMain<IBootstrap>();

            (string data_spacename, string data_instanceurl) = await _Bootstrap.GetServerInfo().ConfigureAwait(false);
            
            tcpRpcClient.Dispose();

            Assert.That(data_spacename, Is.EqualTo(spacename));
            Assert.That(data_instanceurl, Is.EqualTo(instanceurl));
        }

        [TestCase("FabAccessTest", "test.fab-access.space")]
        public async Task mechanisms()
        {
            TcpRpcClient tcpRpcClient = new TcpRpcClient();      
            tcpRpcClient.InjectMidlayer(_InjectSSL);

            tcpRpcClient.Connect(TestEnv.TESTSERVER, TestEnv.TESTSERVER_PORT);
            await tcpRpcClient.WhenConnected;

            IBootstrap _Bootstrap = tcpRpcClient.GetMain<IBootstrap>();

            Assert.Fail("Not Defined");
        }

        [TestCase("User0")]
        public async Task APIConnect(string username)
        {
            API api = new API();
            ConnectionData connectionData = TestEnv.CreateConnetionData(username);
            await api.Connect(connectionData);

            ServerData data = api.ServerData;

            await api.Disconnect();

            ServerData expected = new ServerData
            {
                APIVersion = new FabAccessAPI.Schema.Version()
                {
                    Major = 1,
                    Minor = 0
                },
                ServerName = "bffhd",
                ServerRelease = "TODO",
                SpaceName = "FabAccessTest",
                InstanceURL = "test.fab-access.space"
            };

            Assert.That(data, Is.EqualTo(expected));
        }
    }

        
}
