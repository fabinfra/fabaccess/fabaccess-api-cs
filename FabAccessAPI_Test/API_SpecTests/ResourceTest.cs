﻿using System.Security.Cryptography;
using Capnp.Rpc;
using FabAccessAPI;
using FabAccessAPI.Schema;
using NLog.Time;
using NUnit.Framework;

namespace FabAccessAPI_Test;

[TestFixture]
public class ResourceTest
{

    // [TestCase("UserA", "MachineAPowerable")]
    // public async Task Claim_Powerable_ON_OFF(string username, string resourcename)
    // {
    //     API api = new API();
    //     ConnectionData connectionData = TestEnv.CreateConnetionData(username);
    //     await api.Connect(connectionData).ConfigureAwait(false);

    //     Resource resource = (await api.Session.Resources.GetByName(resourcename).ConfigureAwait(false)).Item1;

    //     IClaim claim = (await resource.Claim.Claim(null).ConfigureAwait(false)).Ok;

    //     Map<IReadOnlyList<byte>, object> traits = await claim.Traits().ConfigureAwait(false);

    //     byte[] oid_trait_powerable = OID.OidStringToByteArray("1.3.6.1.4.1.61783.612.1.1");

    //     object pointer = traits.Entries.First(x => x.Key == oid_trait_powerable).Value;
        
    //     ITraitPowerable traitPowerable = ((Proxy)pointer).Cast<ITraitPowerable>(false);

    //     StatePowerable statePowerable = await traitPowerable.GetState().ConfigureAwait(false);
    //     if(statePowerable.which != StatePowerable.WHICH.Off)
    //     {
    //         Assert.Inconclusive("start state is not the one expected");
    //     }
        
    //     traitPowerable = (await traitPowerable.TurnOn().ConfigureAwait(false)).Ok;

    //     Assert.That(await traitPowerable.GetState().ConfigureAwait(false), Is.EqualTo(StatePowerable.WHICH.On));

    //     traitPowerable = (await traitPowerable.TurnOff().ConfigureAwait(false)).Ok;

    //     Assert.That(await traitPowerable.GetState().ConfigureAwait(false), Is.EqualTo(StatePowerable.WHICH.Off));
    // }

    // [Test(Description="TestID: 00001")]
    // [TestCase("UserA", "MachineA")]
    // public async Task Claim(string username, string resourcename)
    // {
    //     API api = new API();
    //     ConnectionData connectionData = TestEnv.CreateConnetionData(username);
    //     await api.Connect(connectionData).ConfigureAwait(false);

    //     Resource resource = await api.Session.Resources.GetByName(resourcename).ConfigureAwait(false);
    //     Fallible<IClaim, Claimable.IClaimError> result = await resource.Claim.Claim().ConfigureAwait(false);

    //     // TODO: Claim Error State in Case of Success

    //     IClaim claim = result.Ok;

    //     // TODO: Change State

    //     await claim.Disown().ConfigureAwait(false);

    //     await api.Disconnect().ConfigureAwait(false);
    // }

    // [TestCase("UserA", "fabaccess://test.fab-access.space/resources/MachineA")]
    // public async Task GetByUrl(string username, string resourceURN)
    // {
    //     API api = new API();
    //     ConnectionData connectionData = TestEnv.CreateConnetionData(username);
    //     await api.Connect(connectionData).ConfigureAwait(false);

    //     Resource resource = await api.Session.Resources.GetByUrl(resourceURL).ConfigureAwait(false);

    //     Assert.That(resource, Is.Not.Null);

    //     await api.Disconnect().ConfigureAwait(false);
    // }

    // [TestCase("UserA", "urn:fabaccess:resource:MachineA")]
    // public async Task GetByUrn(string username, string resourceURN)
    // {
    //     API api = new API();
    //     ConnectionData connectionData = TestEnv.CreateConnetionData(username);
    //     await api.Connect(connectionData);

    //     Resource resource = await api.Session.Resources.GetByUrn(resourceURN).ConfigureAwait(false);

    //     await api.Disconnect().ConfigureAwait(false);
    // }

    // [TestCase("UserA")]
    // public async Task List(string username)
    // {
    //     API api = new API();
    //     ConnectionData connectionData = TestEnv.CreateConnetionData(username);
    //     await api.Connect(connectionData).ConfigureAwait(false);

    //     IReadOnlyList<Resource> result = await api.Session.Resources.List().ConfigureAwait(false);

    //     await api.Disconnect().ConfigureAwait(false);
    // }

    // [TestCase("UserA")]
    // public async Task Claimed(string username)
    // {
    //     API api = new API();
    //     ConnectionData connectionData = TestEnv.CreateConnetionData(username);
    //     await api.Connect(connectionData).ConfigureAwait(false);

    //     IReadOnlyList<IClaim> result = await api.Session.Resources.Claimed().ConfigureAwait(false);

    //     await api.Disconnect().ConfigureAwait(false);
    // }

    // [TestCase("UserA", "MachineA")]
    // public async Task ClaimedListCheck(string username, string resourcename)
    // {
    //     API api = new API();
    //     ConnectionData connectionData = TestEnv.CreateConnetionData(username);
    //     await api.Connect(connectionData).ConfigureAwait(false);

    //     IReadOnlyList<IClaim> result1 = await api.Session.Resources.Claimed().ConfigureAwait(false);
    //     Assert.That(result1.Count, Is.Zero);

    //     Resource machineA = await api.Session.Resources.GetByName(resourcename).ConfigureAwait(false);
    //     IClaim claim = (await machineA.Claim.Claim().ConfigureAwait(false)).Ok;

    //     IReadOnlyList<IClaim> result2 =  await api.Session.Resources.Claimed().ConfigureAwait(false);    
    //     Assert.That(result2.Count, Is.Not.Zero);        
        
    //     await api.Disconnect().ConfigureAwait(false);
    // }
}
