﻿using FabAccessAPI;
using FabAccessAPI.Schema;
using NUnit.Framework;

namespace FabAccessAPI_Test;

public class Configuration
{
    [Test(Description="Check if user have Permission to discover resource by list or name")]
    [Parallelizable]
    [TestCase("sbarth", "solderstation", true, true)]
    public async Task ResourceDiscover(string username, string resourcename, bool disclose, bool read)
    {
        API api = new API();
        ConnectionData connectionData = TestEnv.CreateConnetionData(username);
        await api.Connect(connectionData).ConfigureAwait(false);

        List<Resource> resources = (List<Resource>)await api.Session.Resources.List().ConfigureAwait(false);
        
        Assert.That(resources.Exists(x => x.Identifier == resourcename), Is.EqualTo(disclose));
        
        Resource resource = (await api.Session.Resources.GetByName(resourcename).ConfigureAwait(false)).Item1;
        Assert.That(resource != null, Is.EqualTo(read));
    }

    [Test(Description="Check if user have Permission to access resource")]
    [Parallelizable]
    [TestCase("sbarth", "solderstation", true, true, true, true, true, true, true)]
    public async Task ResourcePermissions(string username, string resourcename, bool notify, bool interest, bool claim, bool lock_, bool audit)
    {
        API api = new API();
        ConnectionData connectionData = TestEnv.CreateConnetionData(username);
        await api.Connect(connectionData).ConfigureAwait(false);

        IReadOnlyList<Resource> resources = await api.Session.Resources.List().ConfigureAwait(false);
        
        Resource resource = (await api.Session.Resources.GetByName(resourcename).ConfigureAwait(false)).Item1;
        Assert.That(resource, Is.Not.Null);

        Assert.Multiple(() => 
        {
            Assert.That(!((Notifiable_Proxy)resource.Notify).IsNull, Is.EqualTo(notify));
            Assert.That(!((Interestable_Proxy)resource.Interest).IsNull, Is.EqualTo(interest));
            Assert.That(!((Claimable_Proxy)resource.Claim).IsNull, Is.EqualTo(claim));
            Assert.That(!((Lockable_Proxy)resource.Lock).IsNull, Is.EqualTo(lock_));
            Assert.That(!((Auditable_Proxy)resource.Audit).IsNull, Is.EqualTo(audit));  
        });
    }
}
