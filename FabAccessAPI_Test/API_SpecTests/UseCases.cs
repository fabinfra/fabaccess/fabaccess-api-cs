﻿using Capnp.Rpc;
using FabAccessAPI;
using FabAccessAPI.Schema;
using NUnit.Framework;
using static FabAccessAPI.Schema.Claim;

namespace FabAccessAPI_Test;

public class UseCases
{
    [SetUp]
    public async Task SetUp()
    {
        API api = new API();
        ConnectionData connectionData = TestEnv.CreateConnetionData("admin");
        await api.Connect(connectionData).ConfigureAwait(false);

        // TODO: Vendor Reset DB

        await api.Disconnect().ConfigureAwait(false);
    }

    [Test(
        Description = "Niklas Becker wants to use the solderstation, claims the resource, turns the power on and returns the resource"
    )]
    public async Task UseCase001()
    {
        API api = new API();
        ConnectionData connectionData = TestEnv.CreateConnetionData("nbecker");
        await api.Connect(connectionData).ConfigureAwait(false);

        Resource resource = (await api.Session.Resources.GetByName("solderstation").ConfigureAwait(false)).Item1;

        IClaim claim = (await resource.Claim.Claim(null).ConfigureAwait(false)).Ok;

        Map<IReadOnlyList<byte>, object> traits = await claim.Traits().ConfigureAwait(false);

        byte[] oid_trait_powerable = OID.OidStringToByteArray("1.3.6.1.4.1.61783.612.1.1");

        object pointer = traits.Entries.First(x => x.Key == oid_trait_powerable).Value;
        
        ITraitPowerable traitPowerable = ((Proxy)pointer).Cast<ITraitPowerable>(false);        
        traitPowerable = (await traitPowerable.TurnOn().ConfigureAwait(false)).Ok;
        traitPowerable = (await traitPowerable.TurnOff().ConfigureAwait(false)).Ok;

        await claim.Disown().ConfigureAwait(false);

        await api.Disconnect().ConfigureAwait(false);
    }

    [Test(
        Description = "Niklas Becker wants to enter the space through the frontdoor, claims the resource and unlocks it temporarily"
    )]
    public async Task UseCase002()
    {
        API api = new API();
        ConnectionData connectionData = TestEnv.CreateConnetionData("nbecker");
        await api.Connect(connectionData).ConfigureAwait(false);

        Resource resource = (await api.Session.Resources.GetByName("frontdoor").ConfigureAwait(false)).Item1;

        IClaim claim = (await resource.Claim.Claim(null).ConfigureAwait(false)).Ok;

        Map<IReadOnlyList<byte>, object> traits = await claim.Traits().ConfigureAwait(false);

        byte[] oid_trait_doorable = OID.OidStringToByteArray("1.3.6.1.4.1.61783.612.1.2");

        object pointer = traits.Entries.First(x => x.Key == oid_trait_doorable).Value;
        
        ITraitDoorable traitdoorable = ((Proxy)pointer).Cast<ITraitDoorable>(false);     
        traitdoorable = (await traitdoorable.UnlockTemp(null).ConfigureAwait(false)).Ok;

        await claim.Disown().ConfigureAwait(false);

        await api.Disconnect().ConfigureAwait(false);
    }

    [Test(
        Description = "Niklas Becker wants to reserve a the solderstation for 1 hour in 5 seconds"
    )]
    public async Task UseCase003()
    {
        API api = new API();
        ConnectionData connectionData = TestEnv.CreateConnetionData("nbecker");
        await api.Connect(connectionData).ConfigureAwait(false);

        Resource resource = (await api.Session.Resources.GetByName("solderstation").ConfigureAwait(false)).Item1;

        DateTimeOffset now = DateTimeOffset.UtcNow;

        When when = new When()
        {
            Start = new Timestamp()
            {
                Seconds = now.AddSeconds(5).ToUnixTimeSeconds(),
                Nanoseconds = 0
            },
            End = new Timestamp()
            {
                Seconds = now.AddSeconds(5).AddHours(1).ToUnixTimeSeconds(),
                Nanoseconds = 0
            }
        };

        IInterest interest = (await resource.Interest.Reserve(when).ConfigureAwait(false)).Ok;

        await Task.Delay(5*1000 + 1000);

        IClaim claim = await interest.Upgrade().ConfigureAwait(false);

        await claim.Disown().ConfigureAwait(false);

        await api.Disconnect().ConfigureAwait(false);
    }

    [Test(
        Description = "Niklas Becker wants to use the solderstation but Phillipp Blau allready using it. So Niklas Becker joins the queue and gets the claim after Phillipp Blau disowns the claim."
    )]
    public async Task UseCase004()
    {
        API api = new API();
        ConnectionData connectionData = TestEnv.CreateConnetionData("pblau");
        await api.Connect(connectionData).ConfigureAwait(false);

        Resource resource = (await api.Session.Resources.GetByName("solderstation").ConfigureAwait(false)).Item1;

        IClaim claim = (await resource.Claim.Claim(null).ConfigureAwait(false)).Ok;

        API api2 = new API();
        ConnectionData connectionData2 = TestEnv.CreateConnetionData("nbecker");
        await api2.Connect(connectionData2).ConfigureAwait(false);

        Resource resource2 = (await api2.Session.Resources.GetByName("solderstation").ConfigureAwait(false)).Item1;
        
        IInterest interest = (await resource.Interest.Queue().ConfigureAwait(false)).Ok;

        await claim.Disown().ConfigureAwait(false);

        await api.Disconnect().ConfigureAwait(false);

        IClaim claim2 = await interest.Upgrade().ConfigureAwait(false);

        await claim2.Disown().ConfigureAwait(false);

        await api2.Disconnect().ConfigureAwait(false);
    }

    [Test(
        Description = "Julia Schneider needs an instruction from Maik Pfeiffer for the 3D-Printer FDM"
    )]
    public async Task UseCase005()
    {
        API api = new API();
        ConnectionData connectionData = TestEnv.CreateConnetionData("mpfeifer");
        await api.Connect(connectionData).ConfigureAwait(false);

        Resource resource = (await api.Session.Resources.GetByName("3dprinterfdm").ConfigureAwait(false)).Item1;

        IClaim claim = (await resource.Claim.Claim(null).ConfigureAwait(false)).Ok;

        MakeLendableOk makeLendableOk = (await claim.MakeLendable().ConfigureAwait(false)).Ok;

        API api2 = new API();
        ConnectionData connectionData2 = TestEnv.CreateConnetionData("jschneider");
        await api2.Connect(connectionData2).ConfigureAwait(false);

        IClaim claim2 = (await api2.Session.Resources.AcceptToken(makeLendableOk.Token).ConfigureAwait(false)).Ok;

        await claim2.Disown().ConfigureAwait(false);

        await api2.Disconnect().ConfigureAwait(false);

        IReadOnlyList<RestoredResource> restoredResources = await api.Session.Resources.Restore().ConfigureAwait(false);
        IClaim claim3 = restoredResources[0].Claim;
        await claim3.Disown().ConfigureAwait(false);

        await api.Disconnect().ConfigureAwait(false);
    }

    [Test(
        Description = "Julia Schneider uses the WeldingMachine and wants to transfer it to Leonie Fischer"
    )]
    public async Task UseCase006()
    {
        API api = new API();
        ConnectionData connectionData = TestEnv.CreateConnetionData("jschneider");
        await api.Connect(connectionData).ConfigureAwait(false);

        Resource resource = (await api.Session.Resources.GetByName("weldingmachine").ConfigureAwait(false)).Item1;

        IClaim claim = (await resource.Claim.Claim(null).ConfigureAwait(false)).Ok;

        IReadOnlyList<byte> token = (await claim.MakeTransferable().ConfigureAwait(false)).Ok;

        API api2 = new API();
        ConnectionData connectionData2 = TestEnv.CreateConnetionData("lfischer");
        await api2.Connect(connectionData2).ConfigureAwait(false);

        IClaim claim2 = (await api2.Session.Resources.AcceptToken(token).ConfigureAwait(false)).Ok;

        await api.Disconnect().ConfigureAwait(false);

        await claim2.Disown().ConfigureAwait(false);

        await api2.Disconnect().ConfigureAwait(false);
    }

    [Test(
        Description = "Felix Wagner wants to use the circularsaw which depends on the centralsuction"
    )]
    public async Task UseCase007()
    {
        API api = new API();
        ConnectionData connectionData = TestEnv.CreateConnetionData("fwagner");
        await api.Connect(connectionData).ConfigureAwait(false);

        Resource resource = (await api.Session.Resources.GetByName("circularsaw").ConfigureAwait(false)).Item1;

        IClaim claim = (await resource.Claim.Claim(null).ConfigureAwait(false)).Ok;

        IReadOnlyList<IClaim> dependencies = await claim.GetDependencies().ConfigureAwait(false);

        Map<IReadOnlyList<byte>, object> traits2 = await dependencies[0].Traits().ConfigureAwait(false);

        byte[] oid_trait_powerable = OID.OidStringToByteArray("1.3.6.1.4.1.61783.612.1.1");

        object pointer2 = traits2.Entries.First(x => x.Key == oid_trait_powerable).Value;

        ITraitPowerable traitPowerable2 = ((Proxy)pointer2).Cast<ITraitPowerable>(false);        
        traitPowerable2 = (await traitPowerable2.TurnOn().ConfigureAwait(false)).Ok;
        
        Map<IReadOnlyList<byte>, object> traits = await dependencies[0].Traits().ConfigureAwait(false);
        object pointer = traits2.Entries.First(x => x.Key == oid_trait_powerable).Value;

        ITraitPowerable traitPowerable = ((Proxy)pointer).Cast<ITraitPowerable>(false);        
        traitPowerable = (await traitPowerable.TurnOn().ConfigureAwait(false)).Ok;
        traitPowerable = (await traitPowerable.TurnOff().ConfigureAwait(false)).Ok;

        traitPowerable2 = (await traitPowerable2.TurnOff().ConfigureAwait(false)).Ok;

        await claim.Disown().ConfigureAwait(false);

        await api.Disconnect().ConfigureAwait(false);
    }

    [Test(
        Description = "Felix Wagner wants to use the circularsaw and Thomas Naumann wants to use the bandsaw which depends on the centralsuction. Felix Wagner use the circularsaw first, than Thomas Naumann uses the bandsaw and Felix Wagner disowns th circularsaw first."
    )]
    public async Task UseCase008()
    {
        API api = new API();
        ConnectionData connectionData = TestEnv.CreateConnetionData("fwagner");
        await api.Connect(connectionData).ConfigureAwait(false);

        Resource resource = (await api.Session.Resources.GetByName("circularsaw").ConfigureAwait(false)).Item1;

        IClaim claim = (await resource.Claim.Claim(null).ConfigureAwait(false)).Ok;

        IReadOnlyList<IClaim> dependencies = await claim.GetDependencies().ConfigureAwait(false);

        Map<IReadOnlyList<byte>, object> traits2 = await dependencies[0].Traits().ConfigureAwait(false);

        byte[] oid_trait_powerable = OID.OidStringToByteArray("1.3.6.1.4.1.61783.612.1.1");

        object pointer2 = traits2.Entries.First(x => x.Key == oid_trait_powerable).Value;

        ITraitPowerable traitPowerable2 = ((Proxy)pointer2).Cast<ITraitPowerable>(false);        
        traitPowerable2 = (await traitPowerable2.TurnOn().ConfigureAwait(false)).Ok;
        
        Map<IReadOnlyList<byte>, object> traits = await dependencies[0].Traits().ConfigureAwait(false);
        object pointer = traits2.Entries.First(x => x.Key == oid_trait_powerable).Value;

        ITraitPowerable traitPowerable = ((Proxy)pointer).Cast<ITraitPowerable>(false);        
        traitPowerable = (await traitPowerable.TurnOn().ConfigureAwait(false)).Ok;

        API api2 = new API();
        ConnectionData connectionData2 = TestEnv.CreateConnetionData("fwagner");
        await api2.Connect(connectionData2).ConfigureAwait(false);

        Resource resource2 = (await api2.Session.Resources.GetByName("circularsaw").ConfigureAwait(false)).Item1;

        IClaim claim2 = (await resource.Claim.Claim(null).ConfigureAwait(false)).Ok;

        Map<IReadOnlyList<byte>, object> traits3 = await claim2.Traits().ConfigureAwait(false);

        object pointer3 = traits2.Entries.First(x => x.Key == oid_trait_powerable).Value;

        ITraitPowerable traitPowerable3 = ((Proxy)pointer3).Cast<ITraitPowerable>(false);        
        traitPowerable3 = (await traitPowerable3.TurnOn().ConfigureAwait(false)).Ok;

        traitPowerable = (await traitPowerable.TurnOff().ConfigureAwait(false)).Ok;

        await claim.Disown().ConfigureAwait(false);

        await api.Disconnect().ConfigureAwait(false);

        traitPowerable3 = (await traitPowerable3.TurnOff().ConfigureAwait(false)).Ok;

        IReadOnlyList<IClaim> dependencies2 = await claim.GetDependencies().ConfigureAwait(false);
        Map<IReadOnlyList<byte>, object> traits4 = await dependencies2[0].Traits().ConfigureAwait(false);
        object pointer4 = traits4.Entries.First(x => x.Key == oid_trait_powerable).Value;

        ITraitPowerable traitPowerable4 = ((Proxy)pointer4).Cast<ITraitPowerable>(false);
        traitPowerable4 = (await traitPowerable4.TurnOff().ConfigureAwait(false)).Ok;

        await claim2.Disown().ConfigureAwait(false);

        await api2.Disconnect().ConfigureAwait(false);
    }

    [Test(
        Description = "Katharina Abendroth need to repair the bandsaw and locks it for this purpose"
    )]
    public async Task UseCase009()
    {
        Assert.Fail("Not Implemented");
    }

    [Test(
        Description = "Leonie Fischer has forgotten to return the soldering station and Sarah Barth now has to release the resource again"
    )]
    public async Task UseCase010()
    {
        Assert.Fail("Not Implemented");
    }

    [Test(
        Description = "Sarah Barth gives Michael Ziegler an account for this space"
    )]
    public async Task UseCase011()
    {
        Assert.Fail("Not Implemented");
    }
    
    [Test(
        Description = "Lisa Meier wants to use the electricscrewdriver and need to locate it"
    )]
    public async Task UseCase012()
    {
        Assert.Fail("Not Implemented");
    }

    [Test(
        Description = "Julia Schneider had used the cnc and Claudia Neustadt need to check it"
    )]
    public async Task UseCase013()
    {
        Assert.Fail("Not Implemented");
    }
    
    [Test(
        Description = "Julia Schneider wants to use the cnc by card and Sarah Barth checks it after usage"
    )]
    public async Task UseCase014()
    {
        Assert.Fail("Not Implemented");
    }

    [Test(
        Description = "Nikals Becker gets the role rapid prototyping from Sarah Barth"
    )]
    public async Task UseCase015()
    {
        Assert.Fail("Not Implemented");
    }

    [Test(
        Description = "Lara Schmidt wants to get an 3D printer from the printer farm by terminal"
    )]
    public async Task UseCase016()
    {
        Assert.Fail("Not Implemented");
    }

    [Test(
        Description = "Lisa Meier wants to get the lasercutter via an alias name"
    )]
    public async Task UseCase017()
    {
        Assert.Fail("Not Implemented");
    }

    [Test(
        Description = "Tim Fischer wants to use the 3d printer fdm by his project horizon"
    )]
    public async Task UseCase018()
    {
        Assert.Fail("Not Implemented");
    }

    [Test(
        Description = "Niklas Becker wants to use the 3D scanner"
    )]
    public async Task UseCase019()
    {
        Assert.Fail("Not Implemented");
    }

    [Test(
        Description = "Tim Fischer claims the projectroom on his project and wants to share it to his project partner Niklas Becker, so he can use it too"
    )]
    public async Task UseCase020()
    {
        Assert.Fail("Not Implemented");
    }

    [Test(
        Description = "Tim Fischer gets an projectbox for his project horizon by Sarah Barth to store his items this projectbox is also shared with Niklas Becker"
    )]
    public async Task UseCase021()
    {
        Assert.Fail("Not Implemented");
    }

    [Test(
        Description = "Lara Schmidt gets an project box for her personal use by Sarah Barth"
    )]
    public async Task UseCase022()
    {
        Assert.Fail("Not Implemented");
    }

    [Test(
        Description = "Felix Wagner brings his own wood router to the space and can shares it with Lisa Meier"
    )]
    public async Task UseCase023()
    {
        Assert.Fail("Not Implemented");
    }

    [Test(
        Description = "Lisa Meier wants to get the lasercutter via the termial by card"
    )]
    public async Task UseCase024()
    {
        Assert.Fail("Not Implemented");
    }

    [Test(
        Description = ""
    )]
    public async Task UseCase000()
    {
        Assert.Fail("Not Implemented");
    }
}