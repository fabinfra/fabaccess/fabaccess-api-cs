Use Cases
===

# Requierements
+ User0 - User ohne Account
+ UserA - generischer User
+ UserB - generischer User mit gleichen Rollen wir UserA
+ UserC - generischer User ohne Rollen
+ MachineA - frei zugängliche Resource mit festem Ort und Stromanschluss
+ TerminalA - NFC Reader bei MachineA, welche nur MaschinA nutzen kann
+ RolleB - Rolle mit Berechtigungen MachineB zu nutzen
+ MachineB - beschränkt zugängliche Resource mit festem Ort und Stromanschluss
+ TerminalB - NFC Reader bei MachineA, welche nur MachineB mit PIN nutzen kann
+ TerminalB - NFC Reader bei MachineA, welche nur MaschinB mit PIN nutzen kann
+ MachineC - frei zugängliche Resource mit festem Ort und Stromanschluss, benötigt MachineD zum funktionieren
+ MachineD - Maschine mit welche von MachineC gebraucht wird
+ MachineE - Maschine, welche von meheren Nutzer gleichzeitig verwendet werden kann
+ ManagerA - User mit Berechtigung zum Verwalten von Usern und Vergeben von Berechtigungen
+ UserB
+ CardA - FabFireCard von UserA
+ CardB - FabFireCard von UserB
+ Borepin - Client zum Interagieren von BFFH über FabAccess API
+ BFFH - Server mit FabAccess API

# Assumptions
+ die Pin für alle FabFireCards eine User ist die gleiche
+ Terminals wissen für welche Maschine sie eine PIN benötigen
+ Terminals sind immer an einen Satz von Maschinen gebunden und könnten nur diese Ausleihen
+ Maschinen fallen in einen definierten State zurück, wenn der Claim disowned wird oder disown failed, wenn sich einen Maschine in einem bestimmten State befindet
+ User können entweder einen Claim oder einen Interset an der selben Resource haben

# Tests
## Serverdaten
User0 kann von BFFH im Bootstrape sich Informationen zu dem Serverrelease und namen holen und über die InstanceURL, sowie den Spacenamen.

## Accounterzeugung
*User0 geht zu ManagerA und bittet um einen Account*

ManagerA öffnet Borepin, wird automatisch angemeldet
+ öffnet die Nutzerverwaltung
+ beginnt mit dem Anlegen eines neuen Nutzers
+ trägt den Nutzernamen ein
+ fügt zusätzliche Informationen hinzu
+ gerneriert ein Initialpasswort für User0
+ sagt User0 das Passwort / druckt User0 das Passwort aus / erstellt einen QR-Code mit Anmeldedaten / emuliert einen NTAG mit Anmeldedaten

User0 installiert Borepin, öffnet Borepin und
+ trägt die Serveradresse ein
+ verbindet sich mit dem Server
+ logt sich mit Username und Passwort ein

oder
+ scannt die Anmeldedaten per QR-Code / scannt die Anmeldedaten per NFC
+ verbindet sich mit dem Server und logt sich direkt ein

-> BFFH zwingt User0 ein neues Passwort zu setzen
User0 setzt ein neues Passwort
Borepin holt ein Token für die zukünftige Anmeldung des Gerätes

*User0 kann nun Resourcen ausleihen*

## Rollenvergabe
*UserA möchte MachineB verwenden können*

UserA geht zu ManagerA und erhält eine Einweisung für MachineB
ManagerA öffnet Borepin, wird automatisch angemeldet und
+ öffnet die Nutzerverwaltung
+ wählt UserA aus
+ fügt UserA RolleB zu

*UserA kann nun Borepin öffnen, wird automatisch angemeldet und kann MachineB nutzten*

## Claim - TestID:00001
*UserA steht vor MachineA und möchte diese verwenden*

UserA öffnet Borepin, wird automatisch angemeldet und 
+ wählt MachineA in einer Liste aus / scannt den QR-Code von MachineA / scannt den NTAG von MachineA
+ leiht MachineA aus
+ schaltet den Strom von MachineA frei
+ verwendet MachineA
+ gibt MachineA zurück, Strom wird gesperrt vom BFFH

*UserA hat MachineA verwendet*

## Transfer
*UserA möchte UserB die MachineB übergeben*

UserA hatte MachineB ausgeliehen, öffnet Borepin, wird automatisch angemeldet und
+ wählt MachineA in einer Liste aus / scannt den QR-Code von MachineA / scannt den NTAG von MachineA
+ generiet ein Transfer QR-Code / emuliert eine Transfer NTAG

UserB öffnet Borepin, wird automatisch angemeldet und
+ scannt Transfer QR-Code / scannt Transfer NTAG

*UserB kann nun MachineB verwenden, ohne das der Zustand der Maschine sich verändet hat*

## Lend/Instruct
*UserA möchte UserC an MachineB ausbilden*

UserA hatte MachineB ausgeliehen, öffnet Borepin, wird automatisch angemeldet und
+ wählt MachineA in einer Liste aus / scannt den QR-Code von MachineA / scannt den NTAG von MachineA
+ generiet ein Lend QR-Code / emuliert eine Lend NTAG

UserC öffnet Borepin, wird automatisch angemeldet und
+ scannt Lend QR-Code / scannt Lend NTAG
+ verwendet MachineB
+ gibt MachineB zurück

UserA erhält MachineB zurück, prüft MachineB, gibt MachineB zurück

*UserC hat MachineB verwendet, unter Verantwortung von UserA*

## FabFireCard Erzeugung
*UserA geht zu ManagerA und möchte ein FabFireCard erhalten*

ManagerA öffnet Borepin, wird automatisch angemeldet und
+ öffnet die Nutzerverwaltung
+ wählt UserA aus
+ hält eine leere DESFire Karte auf den NFC Scanner
+ legt eine FabFireCard für UserA an und beschreibt diese
+ gibt UserA die FabFireCard - Card A

UserA öffnet Borepin, wird automatisch angemeldet und
+ geht auf seinem Profil
+ setzt eine Pin für die FabFireCard

*UserA hat eine FabFireCard erhalten und eine zusätzliche Pin festgelegt*

## Passwortänderungen
*UserA möchte sein Passwort ändern*

UserA öffnet Borepin, wird automatisch angemeldet und
+ geht auf seinem Profil
+ beginnt mit dem Ändern des Passwort
+ gibt sein altes Passwort ein
+ gibt ein neues Passwort ein

*UserA hat sein Passwort geändert*

## FabFireCard Pin ändern
*UserA möchte seine PIN zur FabFireCard ändern*

UserA öffnet Borepin, wird automatisch angemeldet und
+ geht auf seinem Profil
+ beginnt mit dem Ändern der Pin
+ gibt sein Passwort ein
+ gibt eine neue PIN ein

*UserA hat seine PIN geändert*

## Passwort vergessen
*UserA geht zu ManagerA und bittet darum ein neues Passwort zu erhalten*

ManagerA öffnet Borepin, wird automatisch angemeldet und
+ öffnet die Nutzerverwaltung
+ wählt UserA aus
+ sagt User0 das Passwort / druckt User0 das Passwort aus / erstellt einen QR-Code mit Anmeldedaten / emuliert einen NTAG mit Anmeldedaten

__Gleiches Verhalten wie bei Accounterzeugung__
*UserA hat sein Passwort zurückgesetzt*

## Terminalnutzung
*UserA möchte MachineA mit CardA ausleihen*

UserA steht vor TerminalA und
+ hält CardA auf den NFC Reader

TerminalA meldet sich bei BFFH und
+ leiht MachineA für UserA aus
+ schaltet den Strom frei

UserA steht vor TerminalA und 
+ gibt auf TerminalA ein, MachineA zurückgeben

oder UserA öffnet Borepin, wird automatisch angemeldet und
+ gibt MaschinA zurück

*UserA hat MachineA mit CardA ausgeliehen und verwendet*

## Terminalnutzung mit PIN
*UserA möchte MachineB mit CardA ausleihen*

UserA steht vor TerminalB und
+ hält CardA auf den NFC Reader
-> TerminalB fordert eine PIN
+ tippt PIN ein

TerminalB meldet sich bei BFFH und
+ leiht MachineB für UserA aus
+ schaltet den Strom frei
__Gleiches Verhalten wie bei Accounterzeugung__

*UserA hat MachineA mit CardA ausgeliehen und verwendet*

## Reservieren
*UserA möchte für morgen MachineA reservieren*

UserA öffnet Borepin, wird automatisch angemeldet und
+ wählt MachineA aus Liste aus
+ geht auf reservieren und trägt ein Zeitraum ein

-> UserA erhält zum Begin der Reservierung automatisch den Claim für die MachineA, wenn dieser verfügbar ist

oder

-> UserA erhält zum Begin der Reservierung eine Benachrichtigung, dass die Maschine gerade verwendet wird

UserA geht zu ManagerA und bittet ihn die Maschine freizugeben

ManagerA öffnet Borepin, wird automatisch angemeldet und
+ wählt MachineA aus Liste aus
+ entfernt den Claim

-> UserA erhält automatisch den Claim für MachineA
*UserA kann MachineA im Reservierungszeitraum nutzen*

## Queuing
*UserB verwendet gerade MachineA und UserA möchte als nächster an MachineA*
UserA öffnet Borepin, wird automatisch angemeldet und
+ wählt MachineA aus Liste aus
+ geht auf queuing

UserB gibt MachineA zurück
-> UserA erhält automatisch ein Claim auf MachineA
*UserA kann direkt nach UserB MachineA nutzen*

## Intereset/Reservierung/Queuing aufhebn
*UserA hat einen Interest auf MachineA*
UserA öffnet Borepin, wird automatisch angemeldet und
+ wählt MachineA aus Liste aus
+ beendet Interest

*UserA hat jetzt keinen Interest auf MachineA*

## Benachrichten
*UserA möchte wissen wann die MachineA frei ist*
UserA öffnet Borepin, wird automatisch angemeldet und
+ wählt MachineA aus Liste aus
+ aktiviert Benachrichtigungen für MachineA (mit Optionen, um welchen State es geht)

-> BFFH benachrichtigt UserA, dass die MachineA frei ist
-> Noftiy wird wieder gelöscht

oder 

-> BFFH benachrichtigt UserA, dass die MachineA frei ist
UserA öffnet Borepin, wird automatisch angemeldet und
+ wählt MachineA aus Liste aus
+ deaktiviert Benachrichtigungen für MachineA

*UserA wurde über Änderungen von MachineA benachrichtigt*

## Abhängige Maschinen
*UserA möchte MachineC ausleihen*

serA öffnet Borepin, wird automatisch angemeldet und
+ wählt MachineA aus Liste aus
+ claimed MachineA
-> MachineD wird von BFFH geclaimed

## Kaputte Maschine melden

## Multi Claim

## Claim überschreiben

## Register 