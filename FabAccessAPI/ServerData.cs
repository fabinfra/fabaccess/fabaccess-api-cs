﻿using System;
using System.Collections.Generic;
using FabAccessAPI.Schema;

namespace FabAccessAPI
{
    public class ServerData
    {
        public Schema.Version APIVersion;
        public string ServerName;
        public string ServerRelease;
        public string SpaceName;

        public string InstanceURL;
        public AuthSupported AuthSupported;
    }
}
