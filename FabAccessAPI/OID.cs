﻿using System.Text;

namespace FabAccessAPI;

public class OID
{
    public static byte[] OidStringToByteArray(string oid)
    {
        string[] split = oid.Trim(' ','.').Split('.');
        List<int> retVal = new List<int>();

        for (int a = 0, b = 0, i = 0; i < split.Length; i++)
        {
            if (i == 0)
                a = int.Parse(split[0]);
            else if (i == 1)
                retVal.Add(40 * a + int.Parse(split[1]));
            else
            {
                b = int.Parse(split[i]);

                if (b < 128)
                    retVal.Add(b);
                else
                {
                    retVal.Add(128+(b/128));
                    retVal.Add(b%128);
                }
            }
        }

        byte[] temp = new byte[retVal.Count];

        for (int i = 0; i < retVal.Count; i++)
            temp[i] = (byte)retVal[i];

        return temp;

    }

    public static string OidByteArrayToString(byte[] oid)
    {
        StringBuilder retVal = new StringBuilder();

        for (int i = 0; i < oid.Length; i++)
        {
            if (i == 0)
            {
                int b = oid[0] % 40;
                int a = (oid[0] - b) / 40;
                retVal.AppendFormat("{0}.{1}", a, b);
            }
            else
            {
                if (oid[i] < 128)
                    retVal.AppendFormat(".{0}", oid[i]);
                else
                {
                    retVal.AppendFormat(".{0}", 
                    ((oid[i] - 128) * 128) + oid[i + 1]);
                    i++;
                }
            }
        }

        return retVal.ToString();
    }
}