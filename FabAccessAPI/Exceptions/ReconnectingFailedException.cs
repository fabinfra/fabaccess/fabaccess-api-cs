﻿using System;

namespace FabAccessAPI.Exceptions
{
    public class ReconnectingFailedException : Exception
    {
        public ReconnectingFailedException()
        {

        }

        public ReconnectingFailedException(string message) : base(message)
        {

        }

        public ReconnectingFailedException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
