﻿using System;

namespace FabAccessAPI.Exceptions.SASL
{
    public class BadMechanismException : Exception
    {
        public BadMechanismException()
        {

        }

        public BadMechanismException(string message) : base(message)
        {

        }

        public BadMechanismException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
